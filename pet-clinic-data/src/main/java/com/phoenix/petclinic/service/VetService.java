package com.phoenix.petclinic.service;

import com.phoenix.petclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
