package com.phoenix.petclinic.service.impl;

import com.phoenix.petclinic.model.Vet;
import com.phoenix.petclinic.service.CrudService;
import com.phoenix.petclinic.service.map.AbstractMapService;

public class VetServiceImpl extends AbstractMapService<Vet, Long> implements CrudService<Vet, Long> {
}
