package com.phoenix.petclinic.service;

import com.phoenix.petclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
