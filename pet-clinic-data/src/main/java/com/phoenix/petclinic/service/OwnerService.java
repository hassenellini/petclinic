package com.phoenix.petclinic.service;

import com.phoenix.petclinic.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {

    Owner findByLastName(String lastName);

}
