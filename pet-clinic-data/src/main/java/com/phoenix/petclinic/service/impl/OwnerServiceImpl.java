package com.phoenix.petclinic.service.impl;

import com.phoenix.petclinic.model.Owner;
import com.phoenix.petclinic.service.CrudService;
import com.phoenix.petclinic.service.map.AbstractMapService;

public class OwnerServiceImpl extends AbstractMapService<Owner, Long> implements CrudService<Owner, Long> {

}
