package com.phoenix.petclinic.service.impl;

import com.phoenix.petclinic.model.Pet;
import com.phoenix.petclinic.service.CrudService;
import com.phoenix.petclinic.service.map.AbstractMapService;

public class PetServiceImpl extends AbstractMapService<Pet, Long> implements CrudService<Pet, Long> {
}
